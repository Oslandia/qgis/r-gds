# r_gds - QGIS Plugin

This plugin is a POC for R-GDS considering the 'location of a case' and 'an outage simulation'.
## Generated options

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.  
Static code analisis is based on: both

See also: [contribution guidelines](CONTRIBUTING.md).

### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <https://gitlab.com/Oslandia/qgis/r-gds>
- repository: <https://gitlab.com/Oslandia/qgis/r-gds>

