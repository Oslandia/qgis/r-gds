#! python3  # noqa: E265

"""
    Main plugin module.
"""

# standard

import subprocess
import os
import csv
from qgis.core import QgsApplication
from qgis.gui import QgisInterface
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QToolBar

# project
from r_gds.__about__ import (
    __icon_path__,
    __title__,
    __uri_homepage__,
)
from r_gds.gui.dlg_settings import PlgOptionsFactory
from r_gds.toolbelt import PlgLogger
from r_gds.gui.dlg_locator import ToolbarLocator
from r_gds.gui.dlg_routing import ToolbarRouting

# ############################################################################
# ########## Classes ###############
# ##################################
#! python3  # noqa: E265


script_dir = os.path.dirname(os.path.abspath(__file__))
sql_path = os.path.normpath(os.path.join(script_dir, 'sql'))


class RGdsPlugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.layer = iface.activeLayer()
        self.log = PlgLogger().log
        self.results = None
        self.filter = ToolbarLocator(self.iface)
        self.iface.registerLocatorFilter(self.filter)


    def initGui(self):
        """Set up plugin UI elements."""

        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        self.action_routing = QAction(
            icon=QIcon(QgsApplication.iconPath("join_miter.svg")),
        )
        self.action_routing.triggered.connect(self.routing)

        self.action_export_excel = QAction(
            icon=QIcon(QgsApplication.iconPath("mActionSharingExport.svg")),
        )

        # -- toolbar
        self.toolbar = QToolBar("R-GDS")
        self.toolbar.addAction(self.action_routing)
        self.iface.addToolBar(self.toolbar)


    def routing(self):
        """ Calling the ToolbarRouting class to initiate routing and save 
        result in an instance variable. """

        iface = self.iface
        self.layer = iface.activeLayer()
        feature = self.layer.selectedFeatures()
        if len(feature) == 1:
            dlg_routing = ToolbarRouting(self.iface.mainWindow())
            self.results = dlg_routing.results
            """Retrieving the results of routing for export to an XLSX file."""
            excel_file = 'resultats_iti.csv'
            results = [row[:-1] for row in self.results]
            with open(excel_file, 'w', newline='') as csvfile:
                csv_writer = csv.writer(csvfile)
                csv_writer.writerow(['numero', 'etagpres', 'diametre', 'techpose', 'fonction', 'nprojpos']) 
                for item in results:
                    csv_writer.writerow(item)
            subprocess.run(('start', 'excel', excel_file), shell=True)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""

        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)
        self.iface.deregisterLocatorFilter(self.filter)
        self.iface.removePluginMenu(__title__, self.action_routing)
        del self.action_routing
        del self.action_export_excel
        self.toolbar.deleteLater()
