#! python3  # noqa: E265

"""
   Attributs routing compute  dialog
"""

from PyQt5.QtWidgets import QDialog
from qgis.core import QgsProject, QgsFeatureRequest,QgsGeometry,QgsField,QgsVectorLayer,QgsFeature, QgsStyle, QgsPointXY
from qgis.utils import iface
import os
import sqlite3
from PyQt5.QtCore import QVariant
from shapely import wkt

script_dir = os.path.dirname(os.path.abspath(__file__))
sql_path = os.path.normpath(os.path.join(script_dir, '..', 'sql'))
svg_path =os.path.normpath(os.path.join(script_dir, '..', 'resources','images'))
class ToolbarRouting(QDialog):
    def __init__(self, parent):

        super().__init__(parent)

        layer = iface.activeLayer()
        features = layer.selectedFeatures()
        for feature in features:
            print(feature)
            feature_n = feature['numero']
            feature_f = feature['fonction']
            # print(feature_f)
            self.saveToDb(feature_n, feature_f, feature_n)

    def spatialte_connexion(self):
        conn = sqlite3.connect(f'{sql_path}/data.sqlite')
        connexion = conn.cursor()
        conn.enable_load_extension(True)
        conn.execute("select load_extension('mod_spatialite')")
        return connexion
     
    def saveToDb(self, user_input, feature_f, feature_n):
        layer_name = "Troncon de départ"
        layers = QgsProject.instance().mapLayersByName(layer_name)
    
        if layers:
            QgsProject.instance().removeMapLayer(layers[0].id())

        connexion = self.spatialte_connexion()
        query = f'''SELECT "ogc_fid","numero","materiau","diametre","nprojpos","etagpres","etat","techpose","fonction", ST_AsText(ST_Line_Interpolate_Point((geometry),0.5)) as point_geom from troncon where numero={user_input}
        '''
        connexion.execute(query)
        query = connexion.fetchall()
        layer = QgsVectorLayer("Point?crs=epsg:3948", "Troncon de départ", "memory")
        fields = QgsField()
        fields_name = ["ogc_fid","numero","materiau","diametre","nprojpos","etagpres","etat","techpose","fonction","point_geom"]
        provider = layer.dataProvider()
        layer.startEditing()
        fields = []
        feature = QgsFeature()
        

        for row in query:
            attributes = []
            for idx, value in enumerate(row):
                if fields_name[idx] == "point_geom":
                    geometry = wkt.loads(value)
                    print(geometry)
                    point = QgsPointXY(geometry.x, geometry.y)
                    print(point)
                    feature.setGeometry(QgsGeometry.fromPointXY(point))
                else:
                    field = QgsField(fields_name[idx], QVariant.String)
                    fields.append(field)
                    attributes.append(value)
                print(fields)
                feature.setAttributes(fields)
                provider.addAttributes(fields)      
        feature.setAttributes(attributes)
        print(attributes)
        provider.addFeature(feature)
        QgsProject.instance().addMapLayer(layer)
        layer.commitChanges()
        if feature_f.startswith("Branchement"):
            query = f''' 
            SELECT obj2_numero
                FROM relations 
                WHERE obj1_numero IN (
                    SELECT obj1_numero
                    FROM relations 
                    WHERE obj2_numero = {user_input} 
                    AND (obj1_type = 'TE-BRANC' OR obj1_type = 'OBTURAT')
                )
                AND nom_rel = 'TRONTEBR'
            '''
            connexion.execute(query)
            query = connexion.fetchall()
            with open(f"{sql_path}/requete_fuite_branchement.sql", "r", encoding="utf-8") as file:
                sql_script = file.read()
                sql_script = sql_script.format(user_input=user_input)
            connexion.execute(sql_script)
            self.results = connexion.fetchall()
            self.select_result()
        else:
            with open(f"{sql_path}/requete_fuite_reseau.sql", "r", encoding="utf-8") as file:
                sql_script = file.read()
                sql_script = sql_script.format(user_input=user_input)
            connexion.execute(sql_script)
            self.results = connexion.fetchall()
            self.select_result()
        connexion.close()
        get_styles = QgsStyle.defaultStyle()
        style = get_styles.symbol('effect drop shadow')

        size = 4 
        style.setSize(size)
        layer.renderer().setSymbol(style)
        layer.triggerRepaint()
        layer_tree_layer = iface.layerTreeView().layerTreeModel().rootGroup().findLayer(layer)
        print(layer_tree_layer)
        if layer_tree_layer:
            layer_tree_layer.setCustomProperty("symbolLegend", True)

        layer.commitChanges()
        QgsProject.instance().addMapLayer(layer)
        lyr = QgsProject.instance().mapLayersByName('troncon')[0]
        iface.setActiveLayer(lyr)

    def select_result(self):
        results = self.results
        self.iface = iface
        layer = QgsProject.instance().mapLayersByName('troncon')[0]
        ftr_numero = [n[0] for n in results]
        all_features_to_select = []
        for f in ftr_numero:
            routing_expression = f"numero = '{f}'"
            request = QgsFeatureRequest().setFilterExpression(routing_expression)
            features = [feature.id() for feature in layer.getFeatures(request)]
            all_features_to_select.extend(features)
        layer.selectByIds(all_features_to_select)
        self.iface.mapCanvas().zoomToSelected(layer)