#! python3  # noqa: E265

"""
   Attributs filter locator dialog
"""

from PyQt5.QtWidgets import  QMessageBox
from qgis.core import QgsProject, QgsFeatureRequest, QgsLocatorFilter, QgsLocatorContext, QgsFeedback, QgsLocatorResult
from qgis.utils import iface
from qgis.gui import QgisInterface
import sqlite3
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
sql_path = os.path.normpath(os.path.join(script_dir, '..', 'sql'))


class ToolbarLocator(QgsLocatorFilter):
    """Instantiation of the QgsLocatorFilter tool"""

    def __init__(self, iface: QgisInterface):
        self.iface = iface
        super(QgsLocatorFilter, self).__init__()

    def name(self) -> str:
        """Returns the unique name for the filter. This should be an untranslated \
        string identifying the filter.

        :return: filter unique name
        :rtype: str
        """
        self.__name__ = "R-GDS search"
        return self.__name__

    def clone(self) -> QgsLocatorFilter:
        """Creates a clone of the filter. New requests are always executed in a clone \
        of the original filter.

        :return: clone of the actual filter
        :rtype: QgsLocatorFilter
        """
        return ToolbarLocator(self.iface)

    def displayName(self) -> str:

        """Returns a translated, user-friendly name for the filter.

        :return: user-friendly name to be displayed
        :rtype: str
        """
        self.__title__= " Données de la table Tronçon"
        return self.__title__

    def prefix(self) -> str:
        """Returns the search prefix character(s) for this filter. Prefix a search with \
        these characters will restrict the locator search to only include results from \
        this filter.

        :return: search prefix for the filter
        :rtype: str
        """
        return ""

    def spatialite_connexion(self):
        conn = sqlite3.connect(f'{sql_path}/data.sqlite')
        connexion = conn.cursor()
        conn.enable_load_extension(True)
        conn.execute("select load_extension('mod_spatialite')")
        return connexion
    
    def fetchResults(
        self, user_input: str, context: QgsLocatorContext, feedback: QgsFeedback
    ):
        connexion = self.spatialite_connexion()

    # Ensuring autocomplete activates after 2+ characters.
        if (
            len(user_input) < 1
            or user_input.rstrip() == self.prefix
        ):
            return

        connexion.execute(f"SELECT NPROJPOS, fid FROM TRONCON WHERE nprojpos LIKE '%{user_input}%'")
        NPROJPOS_value = connexion.fetchall()
        seen_values = set() # use of set() to avoid redundant IDs
        for feature in NPROJPOS_value:
            nprojpos_value = feature
            if nprojpos_value not in seen_values:
                seen_values.add(nprojpos_value[0])
                feature = nprojpos_value[0]
                id_value = nprojpos_value[0]
                result = QgsLocatorResult()
                result.displayString = f"Numéro d'affaire: {feature} (ID: {id_value})"
                result.userData = feature
                self.resultFetched.emit(result)

    def triggerResult(self, result):
        """Triggers a filter result from this filter. This is called when one of the \
        results obtained by a call to fetchResults() is triggered by a user. \
        The filter subclass must implement logic here to perform the desired operation \
        for the search result. E.g. a file search filter would open file associated \
        with the triggered result.

        :param result: [description]
        :type result: QgsLocatorResult
        """

        # Create a feature request to find all the sections with the same ID and zoom in on them. 
        feature = result.userData
        print(feature)
        self.iface = iface
        layer = QgsProject.instance().mapLayersByName('troncon')[0]
        if feature:
            NPROJPOS_expression = f"NPROJPOS = '{feature}'"
            request = QgsFeatureRequest().setFilterExpression(NPROJPOS_expression)
            features = [f for f in layer.getFeatures(request)]
            if features:
                layer.selectByExpression(NPROJPOS_expression)
                self.iface.mapCanvas().zoomToSelected(layer)
            self.saveToDb(NPROJPOS_expression)
        
    def saveToDb(self, NPROJPOS_expression):
        """Calling the database with SQLite to select the length of segments by diameter.
        :param NPROJPOS_expression: 
        :type NPROJPOS_expression: str
        """
        connexion = self.spatialite_connexion()
        query = f"SELECT DIAMETRE, SUM(ST_Length(geometry)) AS total_longueur FROM troncon where {NPROJPOS_expression} GROUP BY DIAMETRE;"
        connexion.execute(query)
        results = connexion.fetchall()
        connexion.close()
        self.addResult(results)
         
    def addResult(self, results):
        """show result in a QMessageBox.
        :param results: 
        :type results: list
        """
        print(results)
        html_table = self.create_html_table(results)
        print(html_table)
        msg_box = QMessageBox()
        msg_box.setWindowTitle("Somme des longueurs par diamètres")
        msg_box.setText(f"<h3>Somme des longueurs par diamètre</h3>{html_table}")
        msg_box.exec_()

    def create_html_table(self, results):
        html = """
            <style>
                table {
                    border-collapse: collapse;
                    width: 100%;
                    margin-top: 20px;
                }
                th, td {
                    border: 1px solid black; 
                    padding: 5px;
                    text-align: left;
                }
            </style>
            <table>
                <tr>
                    <th>Diamètre (cm)</th>
                    <th>Longueur (m)</th>
                </tr>
            """
        for diameter, length in results:
            length_float = f"{length:.3f}".replace('.', ',')
            html += f"""
            <tr>
                <td>{diameter}</td>
                <td>{length_float}</td> 
            </tr>
            """
        html += "</table>"
        return html