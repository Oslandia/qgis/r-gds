WITH RECURSIVE reseau AS (
	SELECT 
		numero, 
		t.etagpres, 
		diametre, 
		techpose, 
		fonction, 
		nprojpos, 
		t.GEOMETRY
	FROM 
		troncon t
	WHERE 
		t.NUMERO = (
						WITH RECURSIVE reseau AS (
							SELECT 
								numero, 
								t.etagpres, 
								diametre, 
								techpose, 
								fonction, 
								nprojpos, 
								t.GEOMETRY
							FROM 
								troncon t
							WHERE 
								t.NUMERO = '{user_input}'
							
							UNION
							
							SELECT 
								t.numero, 
								t.etagpres, 
								t.diametre, 
								t.techpose, 
								t.fonction, 
								t.nprojpos, 
								t.GEOMETRY
							FROM 
								reseau re 
							left join relations r1 on re.numero = r1.obj2_numero
							left join robinet ro on r1.obj1_numero = ro.numero
							left join relations r2 on r1.obj1_numero = r2.obj1_numero
							left join troncon as t on r2.obj2_numero = t.numero
							
							where ro.position like '%ouvert%' or ro.position is null AND t.fonction like '%branchement%' 
						), 

						branchement_concerne as (select * from reseau),  

						premier_troncon_sur_reseau as (
						select t.numero from branchement_concerne be
						left join relations r1 on be.numero = r1.obj2_numero
						left join relations r2 on r1.obj1_numero = r2.obj1_numero
						left join troncon as t on r2.obj2_numero = t.numero
						where t.fonction not like '%branchement%')

						select * from premier_troncon_sur_reseau)

	UNION
	
	SELECT 
		t.numero, 
		t.etagpres, 
		t.diametre, 
		t.techpose, 
		t.fonction, 
		t.nprojpos, 
		t.GEOMETRY
	FROM 
		reseau re 
	left join relations r1 on re.numero = r1.obj2_numero
	left join robinet ro on r1.obj1_numero = ro.numero
	left join relations r2 on r1.obj1_numero = r2.obj1_numero
	left join troncon as t on r2.obj2_numero = t.numero
	
	where ro.position like '%ouvert%' or ro.position is null AND t.fonction not like '%branchement%' 
)
select * from reseau

UNION 

select * from (

						WITH RECURSIVE reseau AS (
							SELECT 
								numero, 
								t.etagpres, 
								diametre, 
								techpose, 
								fonction, 
								nprojpos, 
								t.GEOMETRY
							FROM 
								troncon t
							WHERE 
								t.NUMERO = '{user_input}'
							
							UNION
							
							SELECT 
								t.numero, 
								t.etagpres, 
								t.diametre, 
								t.techpose, 
								t.fonction, 
								t.nprojpos, 
								t.GEOMETRY
							FROM 
								reseau re 
							left join relations r1 on re.numero = r1.obj2_numero
							left join robinet ro on r1.obj1_numero = ro.numero
							left join relations r2 on r1.obj1_numero = r2.obj1_numero
							left join troncon as t on r2.obj2_numero = t.numero
							
							where ro.position like '%ouvert%' or ro.position is null AND t.fonction like '%branchement%' 
						) select * from reseau)

